;;; elsa-debug-workaround.el --- Work-arounds and debugging utils for Elsa

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>

;; SPDX-License-Identifier: GPL-3.0-or-later

;; Author: spellcard199 <spellcard199@protonmail.com>
;; Maintainer: spellcard199 <spellcard199@protonmail.com>
;; Keywords: emacs-lisp, elsa
;; Homepage: https://gitlab.com/spellcard199/elsa-debug-workaround
;; Package-Requires: ((emacs "26.1") (elsa "20201011.1950"))
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Version: 0.0.1

;;; Commentary:
;; Elsa is still in alpha, so it's useful to override some behavior for:
;; - debugging Elsa issues
;; - skipping files to make running Elsa faster
;; - working around Elsa's issues
;; How to use:
;;   cask exec elsa --load elsa-debug-workaround.el

;;; Code:

(require 'elsa)
(require 'cl-lib)

(setq debug-on-error t)

(defvar elsa-debug-workaround-skip-file-if-error t
  "If non-nil just skips current file if errors are thrown.
If nil, when an error occurs Elsa stops and `(backtrace)' is printed.")

(defvar elsa-debug-workaround-skip-file-regexps
  '("/company.el"
    "/indium-*[a-z]*.el"
    "/js2-mode.el")
  "List of regexp for files to exclude from Elsa's checking.")

;; (elsa-debug-workaround-skip-file-p :: String -> Nil | Mixed)
(defun elsa-debug-workaround-skip-file-p (file)
  "Iteratively match `elsa-debug-workaround-skip-file-regexps' on FILE.
Returns non-nil when the first regexp matches, nil if none match."
  (cl-do* ((i-max (1- (length elsa-debug-workaround-skip-file-regexps)))
           (i 0 (1+ i))
           (current-regexp
            (nth i elsa-debug-workaround-skip-file-regexps)
            (nth i elsa-debug-workaround-skip-file-regexps))
           (skip (string-match current-regexp file)
		 (string-match current-regexp file)))
      ((or skip (>= i i-max))
       skip)))

;; (elsa-debug-workaround-make-debugger :: String -> Mixed)
(defun elsa-debug-workaround-make-debugger (file)
  "Factory for Custom debugger functions used by Elsa.
This function returns a closure that is the actual debugger function.
This factory pattern is so that the debugger function has access to
the current file Elsa was working on when the error occurred.
Argument FILE is the same `file' argument passed to `elsa-process-file'."
  (lambda (&rest debugger-args)
    (message (concat "ELSA-ERROR: " (prin1-to-string debugger-args)))
    (message (concat "ELSA-ERROR-FILE: " file))
    (message "ELSA-ERROR-BACKTRACE: %s"
             (with-temp-buffer
               (let ((standard-output (current-buffer)))
                 (backtrace)
                 (buffer-string))))))

;; (elsa-debug-workaround--elsa-process-file :: (String -> Mixed) -> String -> Mixed)
(defun elsa-debug-workaround--elsa-process-file (elsa-process-file-func file)
  "Function that can replace `ELSA-PROCESS-FILE-FUNC' when it is adviced.
Argument `ELSA-PROCESS-FILE-FUNC' is the function passed by advicing mechanism.
Argument FILE is the same `FILE' argument that is passed to `ELSA-PROCESS-FILE-FUNC'."
  (unless (stringp file) (error "FILE should be a string"))
  (if (elsa-debug-workaround-skip-file-p file)
      t
    (if elsa-debug-workaround-skip-file-if-error
        (condition-case err
            (funcall elsa-process-file-func
                     file)
          ((error)
           (message (concat file "\n" (prin1-to-string err) "\n"))
           t))
      (let ((debugger (elsa-debug-workaround-make-debugger file)))
        (funcall elsa-process-file-func
                 file)))))

(defun elsa-debug-workaround-process-file-advice-add ()
  "Replace `elsa-process-file' with file-filtering advice."
  (eval-after-load 'elsa
    (advice-add 'elsa-process-file
                :around
                #'elsa-debug-workaround--elsa-process-file)))

(defun elsa-debug-workaround-process-file-advice-remove ()
  "Remove file-filtering advice from `elsa-process-file'."
  (eval-after-load 'elsa
    (advice-remove 'elsa-process-file
                   #'elsa-debug-workaround--elsa-process-file)))

(provide 'elsa-debug-workaround)
;;; elsa-debug-workaround.el ends here
